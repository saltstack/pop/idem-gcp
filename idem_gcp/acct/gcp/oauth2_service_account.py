try:
    from google.oauth2.service_account import Credentials
    import google.auth._service_account_info as service_account_info

    HAS_LIBS = (True,)
except ImportError as e:
    HAS_LIBS = False, str(e)

__virtualname__ = "oauth2"


def __virtual__(hub):
    return HAS_LIBS


def gather(hub):
    """
    Get profile names from encrypted service account data

    Example:
    .. code-block:: yaml

        gcp.oauth2:
          development:
            type: "service_account"
            project_id: "idem_gcp.com:idemgcp"
            private_key_id: "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
            private_key: "-----BEGIN PRIVATE KEY-----\nxxxxxxxxxxxxxxxxxxxxxxxxxxxxxn\n-----END PRIVATE KEY-----\n"
            client_email: "idem-gcp@example.com.iam.gserviceaccount.com"
            client_id: "xxxxxxxxxxxxxxxxxxxxx"
            auth_uri: "https://accounts.google.com/o/oauth2/auth"
            token_uri: "https://oauth2.googleapis.com/token"
            auth_provider_x509_cert_url: "https://www.googleapis.com/oauth2/v1/certs"
            client_x509_cert_url: "https://www.googleapis.com/robot/v1/metadata/x509/idem-gcp%40example.com.iam.gserviceaccount.com"
    """
    sub_profiles = {}
    for profile, ctx in hub.acct.PROFILES.get("gcp.oauth2", {}).items():
        signer = service_account_info.from_dict(
            ctx, require=["client_email", "token_uri"]
        )
        credentials = Credentials(
            signer,
            service_account_email=ctx["client_email"],
            token_uri=ctx["token_uri"],
            project_id=ctx.get("project_id"),
        )

        sub_profiles[profile] = {"credentials": credentials}

    return sub_profiles
