CLI_CONFIG = {}
CONFIG = {
    "gcp_app_credentials": {
        "os": "GOOGLE_APPLICATION_CREDENTIALS",
        "type": str,
        "help": "The full path to your service account private key file",
        "dyne": "idem",
        "default": None,
    },
    "gcp_enable_sockets": {
        "os": "GAE_USE_SOCKETS_HTTPLIB",
        "type": bool,
        "help": "Enable socket support for your app",
        "default": None,
    },
}
SUBCOMMANDS = {}
DYNE = {
    "exec": ["exec"],
    "states": ["states"],
    "tool": ["tool"],
    "acct": ["acct"],
}
