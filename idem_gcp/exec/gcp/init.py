from pprint import pprint
from typing import Set, Dict, Any, List, Callable

from pop.contract import Contracted
from pop.loader import LoadedMod
from pop.hub import Sub
from collections.abc import Mapping

try:
    import googleapiclient.discovery as discovery
    import googleapiclient.discovery_cache as discovery_cache
    import googleapiclient.discovery_cache.file_cache as file_cache
    import googleapiclient.discovery_cache.base as base_cache
    from google.auth.credentials import AnonymousCredentials

    HAS_LIBS = (True,)
except ImportError as e:
    HAS_LIBS = False, str(e)


def __virtual__(hub):
    return HAS_LIBS


# The keys are how the service will appear on the hub, the value is for googleapi.discovery
# TODO dynamically create this list
SERVICES = {
    "crm": "cloudresourcemanager",
    "iam": "iam",
}


def __init__(hub):
    hub.exec.gcp.ACCT = ["gcp"]
    hub.exec.gcp._subs = DynamicAPISubs(hub)


def get_dynamic_api_subs(hub, cache: base_cache.Cache):
    return DynamicAPISubs(hub, cache)


class DynamicMapping(Mapping):
    def __init__(self, hub, cache: base_cache.Cache = None):
        self.hub = hub
        self._cache = cache

    @property
    def cache(self) -> Dict[str, discovery.Resource]:
        if self._cache is None:
            self._cache = discovery_cache.autodetect()
        if not isinstance(self._cache, file_cache.Cache):
            raise NotImplementedError(
                "TODO Iterate over appengine memcache if that's how the cache is stored"
            )

        ret = {}
        f = file_cache.LockedFile(self._cache._file, "r+", "r")
        try:
            f.open_and_lock()
            if f.is_locked():
                cache = file_cache._read_or_initialize_cache(f)
                for url in cache:
                    content, t = cache.get(url, (None, 0))
                    ret[url] = discovery.build_from_document(
                        content, base=url, credentials=AnonymousCredentials()
                    )
        except Exception as e:
            file_cache.LOGGER.warning(e, exc_info=True)
        finally:
            f.unlock_and_close()
        return ret

    def __getitem__(self, url: str) -> discovery.Resource:
        return self.cache[url]

    def __getattr__(self, item):
        try:
            return self[item]
        except KeyError:
            raise AttributeError(f"Unknown attribute {item}")

    @property
    def cached_values(self):
        raise NotImplementedError()

    def __iter__(self):
        return iter(self.cached_values)

    def __len__(self) -> int:
        return len(self.cached_values)

    def get_cache_resource(self, api_version: str, service: str, *sub_item):
        for resource in self.cache.values():
            if (
                resource._resourceDesc["version"] == api_version
                and resource._resourceDesc["name"] == service
            ):
                for item in sub_item:
                    resource = getattr(resource, item)
                return resource

    def __add__(self, other: "DynamicMapping"):
        ret = {}
        for key, value in self.items():
            ret[key] = value
        for key, value in other.items():
            ret[key] = value
        return ret


class DynamicAPISubs(DynamicMapping):
    def __init__(self, hub, cache: base_cache.Cache = None):
        super().__init__(hub, cache)
        self.sub = hub.exec.gcp

    def __getitem__(self, api_version: str) -> Sub:
        sub = Sub(hub=self.hub, subname=api_version, root=self.sub, init=False)
        sub._subs = DynamicBaseResourceSub(self.hub, sub, api_version)
        return sub

    @property
    def cached_values(self) -> Set[str]:
        return {x._resourceDesc["version"] for x in self.cache.values()}


class DynamicBaseResourceSub(DynamicMapping):
    def __init__(self, hub, sub: Sub, api_version):
        super().__init__(hub)
        self.api_version = api_version
        self.sub = sub

    def __getitem__(self, resource: str) -> Sub:
        sub = Sub(hub=self.hub, subname=resource, root=self.sub, init=False)
        # sub._loaded = DynamicLoader(self.hub, sub, self.api_version)
        sub._loaded = ResourceSubLoader(
            self.hub, sub, self.api_version, base_resource=resource
        )
        return sub

    @property
    def cached_values(self) -> Set[str]:
        return {
            resource._resourceDesc["name"]
            for resource in self.cache.values()
            if resource._resourceDesc["version"] == self.api_version
        }


class ResourceSubLoader(DynamicMapping):
    def __init__(
        self,
        hub,
        sub: Sub,
        api_version: str,
        base_resource: str,
        resources: List[str] = None,
    ):
        super().__init__(hub)
        self.base = base_resource
        self.resources = resources or []
        self.api_version = api_version
        self.sub = sub

    def __getitem__(self, resource: str) -> "ContractMod":
        resources = self.resources + [resource]
        loaded = ContractMod(
            self.hub,
            sub=self.sub,
            api_version=self.api_version,
            base_resource=self.base,
            resources=resources,
        )
        return loaded

    @property
    def cached_values(self) -> Set[str]:
        ret = set()
        for resource in self.cache.values():
            if not resource._resourceDesc["version"] == self.api_version:
                continue
            if not resource._resourceDesc["name"] == self.base:
                continue

            for r in self.resources:
                resource = getattr(resource, r)()

            resources = resource._resourceDesc.get("resources", {})
            ret.update(resources.keys())

        return ret


class ContractMod(LoadedMod):
    def __init__(
        self,
        hub,
        sub: Sub,
        api_version: str,
        base_resource: str,
        resources: List[str] = None,
    ):
        super().__init__(name=resources[-1])
        self.hub = hub
        self.sub = sub
        self.api_version = api_version
        self.base = base_resource
        self.resources = resources or []

    def __getattr__(self, sub_name: str):
        resources = self.resources + [sub_name]
        sub = Sub(hub=self.hub, subname=sub_name, root=self.sub, init=False)
        sub._loaded = ResourceSubLoader(
            self.hub, sub, self.api_version, base_resource=sub_name, resources=resources
        )
        loaded = ContractMod(
            self.hub,
            sub=sub,
            api_version=self.api_version,
            base_resource=self.base,
            resources=resources,
        )
        return loaded

    def __call__(self, ctx, *args, **kwargs):
        resource = discovery.build(
            serviceName=self.base,
            version=self.api_version,
            credentials=ctx.acct.credentials,
        )
        for r in self.resources:
            resource = getattr(resource, r)()

        return resource.execute()


class DynamicContractedResources(DynamicMapping):
    def _wrap_method(self, name: str) -> Callable:
        def resource(ctx, *args, **kwargs):
            _resource = discovery.build(
                serviceName=self.base,
                version=self.api_version,
                credentials=ctx.acct.credentials,
            )
            for r in self.resources:
                _resource = getattr(_resource, r)()
            func = getattr(_resource, name)
            request = func(*args, **kwargs)
            return request.execute()

        return resource

    def __init__(
        self,
        hub,
        sub: Sub,
        api_version: str,
        base_resource: str,
        resources: List[str] = None,
    ):
        super().__init__(hub)
        self.base = base_resource
        self.resources = resources
        self.api_version = api_version
        self.sub = sub

    def __getitem__(self, resource: str) -> Contracted:
        contracts = []
        ref = ""
        return Contracted(
            self.hub,
            contracts,
            func=self._wrap_method(resource),
            ref=ref,
            name=resource,
            implicit_hub=False,
        )

    @property
    def cached_values(self) -> Dict[str, Any]:
        for resource in self.cache.values():
            if not resource._resourceDesc["version"] == self.api_version:
                continue
            if not resource._resourceDesc["name"] == self.base:
                continue

            resource_attrs = resource._resourceDesc["resources"]
            if self.resources:
                # Travel down the chain of resources until we get to the thing we need
                for r in self.resources:
                    if "resources" in resource_attrs.get(r, {}):
                        resource_attrs = resource_attrs[r]["resources"]
                    elif "methods" in resource_attrs.get(r, {}):
                        return resource_attrs[r]["methods"]
                    else:
                        break
        return {}
