__sub_alias__ = ["crm"]


def __init__(hub):
    hub.exec.gcp.cloud_resource_manager.SERVICE_CACHE = {}


def service(hub, ctx):
    name = repr(ctx.acct.credentials)
    if name not in hub.exec.gcp.cloud_resource_manager.SERVICE_CACHE:
        hub.exec.gcp.cloud_resource_manager.SERVICE_CACHE[
            name
        ] = hub.tool.gcp.discovery.build(ctx, "cloudresourcemanager")
    return hub.exec.gcp.cloud_resource_manager.SERVICE_CACHE[name]
