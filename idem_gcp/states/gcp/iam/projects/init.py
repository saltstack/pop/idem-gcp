def service(hub, ctx):
    s = hub.exec.gcp.iam.init.service(ctx)
    return s.projects()
