def _wrap_iam_client(hub, target: str):
    def _resource_manager(ctx, *args, **kwargs):
        service = hub.exec.gcp.iam.init.service(ctx)
        with service.roles() as roles:
            # print(roles._dynamic_attrs)
            request = getattr(roles, target)(*args, **kwargs)
            return request.execute()

    return _resource_manager


def __func_alias__(hub):
    out = {}
    for func in (
        "get",
        "list",
        "list_next",
        "queryGrantableRoles",
        "queryGrantableRoles_next",
    ):
        out[func] = _wrap_iam_client(hub, func)
    return out
