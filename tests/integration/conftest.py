import dict_tools
from unittest import mock
import pytest
from typing import Any, Dict, List
import google.auth.credentials as google_auth


@pytest.fixture(scope="session", autouse=True)
def acct_subs() -> List[str]:
    return ["gcp"]


@pytest.fixture(scope="session", autouse=True)
def acct_profile() -> str:
    return "test_development_idem_gcp"


@pytest.fixture(scope="session")
def hub(hub):
    hub.pop.sub.add(dyne_name="idem")

    with mock.patch(
        "sys.argv",
        ["idem", "state"],
    ):
        hub.pop.config.load(["idem", "acct"], "idem", parse_cli=True)

    yield hub


@pytest.mark.asyncio
@pytest.fixture(scope="module")
async def ctx(hub, acct_subs: List[str], acct_profile: str) -> Dict[str, Any]:
    ctx = dict_tools.data.NamespaceDict(run_name="test", test=False)

    # Add the profile to the account
    hub.acct.init.unlock(hub.OPT.acct.acct_file, hub.OPT.acct.acct_key)
    ctx.acct = await hub.acct.init.gather(acct_subs, acct_profile)

    # Default to anonymous credentials
    if "credentials" not in ctx.acct:
        hub.log.warning(
            "Running the tests with anonymous credentials, create a profile named 'test_development_idem_gcp` with acct to use real credentials"
        )
        ctx.acct = await hub.acct.init.gather(acct_subs, "anonymous")

    # Test if the created ctx is functional; if not then skip all the integration tests that use it
    assert isinstance(ctx.acct.credentials, google_auth.Credentials)
    yield ctx
