from pprint import pprint
import tests.data
import pathlib
import googleapiclient.discovery_cache.file_cache as file_cache


def test_basic_discovery(hub, ctx):
    projects = hub.exec.gcp.v1.cloudresourcemanager.projects.list(ctx)
    pprint(projects)


def test_recursive_discovery(hub, ctx):
    projects = hub.exec.gcp.v1.iam.projects.serviceAccounts.list(ctx)
    print(projects)


def test_cache(hub):
    cache = file_cache.cache
    cache._file = pathlib.Path(tests.data.__file__).parent.joinpath(
        "google-api-python-client-discovery-doc.cache"
    )
    dynamic_sub = hub.exec.gcp.init.get_dynamic_api_subs(cache=cache)

    hub.pop.sub.add(dyne_name="tree")
    tree = hub.tree.init.recurse(dynamic_sub.sub)
    # Reduce the details in the tree
    hub.graph.GRAPH = "simple"
    tree = hub.graph.init.recurse(tree)
    print("-" * 100)
    print("-" * 100)
    print(hub.output.nested.display(tree["v1"]["iam"]))
    print("-" * 100)

    assert set(tree.v1.iam.projects.roles["functions"].keys()) == {
        "create",
        "delete",
        "get",
        "list",
        "patch",
        "undelete",
    }
    assert set(tree.v1.iam.projects.serviceAccounts["functions"].keys()).issuperset(
        {"get", "create", "delete", "list"}
    )
    assert set(
        tree.v1.iam.projects.serviceAccounts.get("keys")["functions"].keys()
    ) == {"get", "delete", "list", "upload", "create"}
